import { NEWTUROANGULARPage } from './app.po';

describe('new-turo-angular App', () => {
  let page: NEWTUROANGULARPage;

  beforeEach(() => {
    page = new NEWTUROANGULARPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
