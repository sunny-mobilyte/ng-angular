import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AgmCoreModule } from "angular2-google-maps/core";

import { AppComponent } from './app.component';
import { HeaderComponent } from '../components/header/component';
import { HomeSearcBoxComponent } from '../components/HomeSearchBox/component';

import { SearchService } from '../services/services';
//import { PersonComponent } from '../components/person/component';

@NgModule({
     declarations: [
          AppComponent,
          HeaderComponent,
          HomeSearcBoxComponent
     ],
     imports: [
          AgmCoreModule.forRoot({
               apiKey : 'AIzaSyCqjtdscZotjnwze8VGNTGz17TuWsFjE4c',
               libraries: ["places"]
          }),
          BrowserModule,
          FormsModule,
          ReactiveFormsModule,
          HttpModule
     ],
     providers: [SearchService],
     bootstrap: [AppComponent]
})
export class AppModule { }
