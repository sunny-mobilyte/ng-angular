import { Component, NgModule, NgZone, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { AgmCoreModule, MapsAPILoader } from 'angular2-google-maps/core';
import {} from '@types/googlemaps';


import { SearchService } from '../../services/services';
//import { EmitterService } from '../../services/emitter.service';


 var google;
@Component({
     selector: 'home-search-box',
     styles: [`
          .sebm-google-map-container {
               height: 300px;
          }
          `],
          templateUrl: './component.html'
     })

     export class HomeSearcBoxComponent implements OnInit {

          public latitude: number;
          public longitude: number;
          public searchControl: FormControl;
          public zoom: number;

          @ViewChild("search")
          public searchElementRef: ElementRef;

          constructor(
               private mapsAPILoader: MapsAPILoader,
               private ngZone: NgZone,
               private SearchService : SearchService

          ) {}



          ngOnInit() {

               //set google maps defaults
               this.zoom = 4;
               this.latitude = 39.8282;
               this.longitude = -98.5795;

               //create search FormControl
               this.searchControl = new FormControl();

               //set current position
               this.setCurrentPosition();

               //load Places Autocomplete
               this.mapsAPILoader.load().then(() => {
                    let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                         types: ["address"]
                    });
                    autocomplete.addListener("place_changed", () => {
                         this.ngZone.run(() => {
                              //get the place result
                              let place: google.maps.places.PlaceResult = autocomplete.getPlace();

                              //verify result
                              if (place.geometry === undefined || place.geometry === null) {
                                   return;
                              }

                              //set latitude, longitude and zoom
                              this.latitude = place.geometry.location.lat();
                              this.longitude = place.geometry.location.lng();
                              this.zoom = 12;
                         });
                    });
               });
               this.getCars();
          }

          getCars () {
               this.SearchService.search().subscribe(
                    cars => {
                         // Emit list event
                         //this.items = cars;
                    },
                    err => {
                         // Log errors if any
                         console.log(err);
                    });
          }

               private setCurrentPosition() {
                    if ("geolocation" in navigator) {
                         navigator.geolocation.getCurrentPosition((position) => {
                              this.latitude = position.coords.latitude;
                              this.longitude = position.coords.longitude;
                              this.zoom = 12;
                         });
                    }
               }
          }
